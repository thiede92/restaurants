import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutComponent } from './components/about/about.component';

import { RestaurantListComponent } from './components/restaurant-list/restaurant-list.component';
import { RestaurantComponent } from './components/restaurant/restaurant.component';

const routes: Routes = [
  { path: '', redirectTo: 'restaurants', pathMatch: 'full' },
  { path: 'restaurants', component: RestaurantListComponent, data: { state: 'restaurants'} },
  { path: 'restaurant/:id', component: RestaurantComponent, data: { state: 'restaurant'}  },
  { path: 'about', component: AboutComponent, data: { state: 'about'}  }
];

@NgModule({
  imports: [RouterModule.forRoot(
    routes,
  { scrollPositionRestoration: 'enabled' }
    )],
  exports: [RouterModule]
})
export class AppRoutingModule { }
