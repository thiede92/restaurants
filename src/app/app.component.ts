import { Component } from '@angular/core';
import  {trigger, transition, useAnimation}  from  "@angular/animations";
import { moveFromBottomFade } from 'ngx-router-animations';
import { moveFromTopFade } from 'ngx-router-animations';
import { moveFromLeftFade } from 'ngx-router-animations';
import { moveFromRightFade } from 'ngx-router-animations';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations:  [
    trigger('moveFromBottomFade',  [ transition('restaurants => restaurant', useAnimation(moveFromBottomFade))]),
    trigger('moveFromTopFade',  [ transition('restaurant => restaurants', useAnimation(moveFromTopFade))]),
    trigger('moveFromLeftFade',  [ transition('about => *', useAnimation(moveFromLeftFade))]),
    trigger('moveFromRightFade',  [ transition('* => about', useAnimation(moveFromRightFade))]),
    ]
})
export class AppComponent {
  title = 'restaurants';

  getState(outlet: any)  {
		return outlet.activatedRouteData.state;
	}
}
