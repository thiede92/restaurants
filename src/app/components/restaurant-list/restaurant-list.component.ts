import { Component, OnInit } from '@angular/core';
import { RestaurantService } from 'src/app/services/restaurant.service';

@Component({
  selector: 'app-restaurant-list',
  templateUrl: './restaurant-list.component.html',
  styleUrls: ['./restaurant-list.component.scss']
})
export class RestaurantListComponent implements OnInit {
  restaurantList?: any[];
  imgPath: string = "../../../assets/";
  inputValue: string = "";

  constructor(private restaurantService: RestaurantService) {
  }

  ngOnInit(): void {
    this.getRestaurants();
  }

  getRestaurants() {
    this.restaurantService.getRestaurants().subscribe((res) => {
      this.restaurantList = res.filter((x) => x.imgName = this.imgPath + x.imgName);
    })
  }

  handlePhoneClick(e: Event) {
    e.stopPropagation();
  }

  searchRestaurant(item: any) {
    return item.name.toLowerCase().includes(this.inputValue.toLowerCase())
  }

}

