import { Component, OnInit } from '@angular/core';
import { Restaurant } from 'src/app/models/Restaurant';
import { RestaurantService } from 'src/app/services/restaurant.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-restaurant',
  templateUrl: './restaurant.component.html',
  styleUrls: ['./restaurant.component.scss']
})
export class RestaurantComponent implements OnInit {
  id: any;
  imgPath: string = "../../../assets/";
  restaurant: Restaurant = {
    id: "",
    name: "",
    address: {
      street: "",
      number: "",
      zipCode: "",
      city: "",
    },
    phone: "",
    email: "",
    website: "",
    imgName: ""
  };

  constructor(
    private restaurantService: RestaurantService,
    private activatedRoute: ActivatedRoute
  ) { }

  async ngOnInit() {
    this.id = await this.activatedRoute.snapshot.paramMap.get("id");
    this.getRestaurant(this.id);
  }

  getRestaurant(id: number) {
    this.restaurantService.getRestaurant(id).subscribe(res => {
      res.imgName = this.imgPath + res.imgName;
      this.restaurant = res;
    })
  }

}
