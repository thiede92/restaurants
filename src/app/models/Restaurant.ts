import { Reviews } from './Reviews';

export interface Restaurant {
    id: string;
    name: string;
    address: {
        street: string;
        number: string;
        zipCode: string;
        city: string;
    };
    phone: string;
    email: string;
    website: string;
    imgName: string;
    reviews?: Reviews;
}