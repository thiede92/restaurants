import { Data } from '@angular/router';

export interface Review {
    id: string;
    author: string;
    addDate: Data;
    description: string;
}