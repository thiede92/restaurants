import { Review } from './Review';

export interface Reviews {
    restaurantId: string;
    reviews: Review[];
}