import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Restaurant } from '../models/Restaurant';

@Injectable({
  providedIn: 'root'
})
export class RestaurantService {
  apiDIR: string = 'http://localhost:3000/restaurants/'

  constructor(private http: HttpClient) { }

  getRestaurant(id: number): Observable<Restaurant> {
    return this.http.get<Restaurant>(this.apiDIR + id)
  }

  getRestaurants(): Observable<Restaurant[]> {
    return this.http.get<Restaurant[]>(this.apiDIR)
  }
  
}
